# AWS Workdir 

A collection of useful AWS scripts, knowledge and tools. 

michael.anckaert@sinax.be

https://www.sinax.be

Last Update: 2018-07-04

## Docs

The *docs* folder contains snippets of AWS wisdom, knowledge and information.

## Tools

A [currated list of tools](tools.md) to make working with AWS better than it already is.

## Scripts

The *scripts* folder contains useful scripts for working with AWS and AWS 
resources.

* **list-ecs-instances** List EC2 instance data for all container instances in
an ECS cluster

* **awsparam** Fetch a parameter value from AWS SSM Parameter store to easily use in 
scripts and configuration files.

* **describe-kms-keys** Describe all KMS keys in the current AWS account

* **security-checkup** Run a checkup on IAM users and their attributes (access keys, MFA, login profiles)

## Working with AWS cli profiles

The default profile in ~/.aws/credentials is set to an invalid ACCESS_KEY and 
SECRET_KEY to ensure that we don't accidentally run any commands using the cli
without selecting a correct AWS profile.

### Selecting the correct AWS profile

Set the AWS_PROFILE environment variable to the name of an AWS credential or
profile. 

    export AWS_PROFILE=<profilename>

A useful addition is the AWS plugin for Oh My Zsh. This gives you the **asp** 
and **agp** commands. 

The **asp** command stands for *AWS Set Profile* and sets the correct env
variables to use a certain profile. 

Use the **agp** command (*AWS Get Profile*) to query for the selected AWS 
profile.

## Using the Generate CLI Skeleton and CLI Input JSON functionality

When running an AWS command you can add the --generate-cli-skeleton option to 
output JSON data that can be passed to that command using the --cli-input-json
parameter. This allows you to assembly extensive commands with ease. 

A short example

    aws ecs list-container-instances --generate-cli-skeleton > list-instances.json

You can then edit the list-istances.json file and enter the required values. Next
you can send that file as input to the list-container-instances command:

    aws ecs list-container-instances --cli-input-json file://list-instances.json

