# AWS related tools

## AWS Command Line Interface (cli)

The official AWS Command Line Interface. 

https://aws.amazon.com/cli/

## awsless

`awless` is a powerful, innovative and small surface command line interface (CLI) to manage Amazon Web Services.

https://github.com/wallix/awless/wiki

## AWS Inventory

A single-file that shows the whole inventory of your AWS services on a single page.

https://github.com/devops-israel/aws-inventory

## Helpfull utilities

### JQ

jq is like sed for JSON data - you can use it to slice and filter and map and transform structured data with the same ease that sed, awk, grep and friends let you play with text.

https://stedolan.github.io/jq/
