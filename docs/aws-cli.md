# AWS CLI 

## Filtering output from the AWS CLI

Documentation about filtering CLI output can be found here: 
https://docs.aws.amazon.com/cli/latest/userguide/controlling-output.html#controlling-output-filter

### A practical example

The following command describes the container instances in an ECS cluster:

    $ aws ecs describe-container-instances --cluster $CLUSTER --container-instances $CONTAINER_ID

Finding the data you want in the output of the previous command can be... difficult. A sample run of the command above returned 193 lines of JSON data.

Luckily the AWS CLI accepts a *--query* parameter that we can use to filter the
output. Let's give it a try:

    $ aws ecs describe-container-instances --cluster $CLUSTER --container-instances *CONTAINER_ID --query 'containerInstances[0].{ec2InstanceId:ec2InstanceId}'                        

    {
        "ec2InstanceId": "i-0ef71eff558f39b07"
    }

This looks much better. Here are some extra examples. 

    $ aws ec2 describe-instances --query 'Reservations[*].Instances[*].{InstanceId:InstanceId,PublicIpAddress:PublicIpAddress}'

    $ aws ecs describe-container-instances --cluster dewaele-iam --container-instances 36e29e1c-7bdb-4960-8278-2e3022742f18 --query 'containerInstances[0].{ec2InstanceId:ec2InstanceId,agentVersion:versionInfo.agentVersion}

    $ aws ecs describe-container-instances --cluster dewaele-iam --container-instances 36e29e1c-7bdb-4960-8278-2e3022742f18 --query 'containerInstances[0].{ec2InstanceId:ec2InstanceId,vpc:attributes[?name==`ecs.vpc-id`].value}



