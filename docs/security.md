# AWS Security

## Checking up on Access Key usage

    $ aws iam get-access-key-last-used --access-key-id YOURKEYIDHERE
    {
        "UserName": "JoeUser", 
        "AccessKeyLastUsed": {
            "Region": "eu-central-1", 
            "ServiceName": "s3", 
            "LastUsedDate": "2018-03-05T10:43:00Z"
        }
    }
