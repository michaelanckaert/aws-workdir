# CloudFormation use with AWS CLI

A sample CloudFormation template can be found in the file *BasicWebserverInVPC.json* 

## Creating a new stack

    $ aws cloudformation create-stack --stack-name BasicWebserver --template-body file://BasicWebserverInVPC.json --parameters ParameterKey=InstanceType,ParameterValue=t2.micro ParameterKey=KeyName,ParameterValue=dew-administrator ParameterKey=SSHLocation,ParameterValue=0.0.0.0/0 --tags 

## Getting status of a stack

    $ aws cloudformation describe-stacks --stack-name BasicWebserver --query 'Stacks[*].{StackId:StackId,StackStatus:StackStatus}'

## List stack resources

    $ aws cloudformation list-stack-resources --stack-name BasicWebserver

## Viewing stack outputs
    $ aws cloudformation describe-stacks --stack-name BasicWebserver --query 'Stacks[*].{Outputs:Outputs}'

## Deleting a stack

    $ aws cloudformation delete-stack --stack-name BasicWebserver
