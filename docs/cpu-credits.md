# CPU Credits

## What are CPU Credits

CPU credits are a construct of AWS to manage/allow CPU bursting.

The way they work is that AWS determines a baseline performance for every instance type. A t2.small instance for example has a baseline performance of 20% CPU usage.

Let's assume your instances uses only 5% CPU for a certain period, then you are given CPU credits in a 'balance account'. When your CPU usage goes above 20% you use CPU credits to 'pay' for this usage. The more credits you have saved the longer you can 'burst' above the baseline performance.

Note that this only applies to T2 instance types. Other instances types don't work with CPU credits. So you should determine your instance type depending on the workload (and resource usage) of your application. More information can be found here: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/general-purpose-instances.html

You can find all AWS document about CPU credits here: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/t2-credits-baseline-concepts.html#cpu-credits

## Get CPU Credit Balance

    $ aws cloudwatch get-metric-statistics --namespace AWS/EC2 --metric-name CPUCreditBalance --start-time 2018-02-01T00:00:00Z --end-time 2018-02-01T23:59:59Z --period 3600 --statistics Average --dimensions Name=InstanceId,Value=i-017e1cbc3aaff4779
